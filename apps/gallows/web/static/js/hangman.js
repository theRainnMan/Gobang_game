import {Socket} from "phoenix"
import $        from "jquery"

//import Gallows  from "./gallows"

export default class Hangman {

    constructor() {
        this.setupDOM()
        this.channel = this.join_channel();
        this.setupEventHandlers(this.channel)
        this.channel.push("get_status", {})
        this.i = 1
        //this.gallows = new Gallows()
    }

    setupDOM() {
        this.input   = $(".entry input")
        this.drawing = $(".drawing")
        this.guessed = $(".guessed")
        this.guessed_letters = $(".guessed .letters")
        this.word    = $(".word")
        this.tb    = $("td")
    }

    join_channel() {
        let socket = new Socket("/socket", { logger: Hangman.my_logger })
        socket.connect()
        let channel = socket.channel("hangman:game")
        channel.join()
        return channel
    }

    setupEventHandlers(channel) {
        //channel.on("status", msg => { this.update_status(msg) })
        //this.input.on("keypress", (ev) =>  this.handle_keypress(ev))
        //$("#tb11").on("click", alert("fffff"))
        //this.tb.on("click", alert("atrrrrr"))
        //$('#xyz').click(alert("xxxxxx"))
        //this.bt.on("keypress", alert("atrrrrr"))
        //alert(this.tb)
        //this.input.on("click", alert("xxx"))

        this.tb.on("click", (ev) =>  this.handle_click(ev))

    }

    handle_keypress(event) {
        this.input.prop('disabled', true)
        this.channel.push("guess", { letter: event.key })
    }

    handle_click(event) {

        //this.input.prop('disabled', true)
        //this.channel.push("guess", { letter: event.key })
        //$(".drawing").on()
        //event.originalEvent.explicitOriginalTarget.style.background = 'black'
        var myCol = event.originalEvent.explicitOriginalTarget.cellIndex
        if (myCol === undefined) {
            return
        }
        //backgroundImage
        var Row = event.originalEvent.explicitOriginalTarget.parentElement.rowIndex
        //console.log(event)
        if(event.originalEvent.explicitOriginalTarget.className == ""){
          if(this.i%2 == 1){
            event.originalEvent.explicitOriginalTarget.className = "black"
          }
          else {
            event.originalEvent.explicitOriginalTarget.className = "white"
          }
          this.channel.push("go", { row: Row, col: myCol, turn: this.i % 2})
          //  alert(myRow)
          this.i++
        }
      }

    update_status(msg) {

        this.input.val("")
        this.input.prop('disabled', false)
        this.input.focus()

        if (msg.used_so_far.length > 0) {
            this.guessed_letters.text(msg.used_so_far)
            this.guessed.show(500)
        }

        this.word.text(msg.word)

        switch (msg.status) {
        case "black_won":
            alert("black_won")
            this.entry.hide()
            break
        case "white_won":
            alert("white_won")
            $(".entry").hide()
        default:
            //this.gallows.display_for(msg.turns_left)
            //hua
            //$(".drawing")
            //
        }
    }

    static my_logger(kind, msg, data) {
        console.log(`Socket: ${kind}: ${msg}`, data)
    }
}
