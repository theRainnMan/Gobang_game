defmodule Hangman do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    IO.puts "STARTING"
    children = [
      
      supervisor(Hangman.GameSupervisor, [], [])
    ]

    opts = [strategy: :one_for_all, name: Hangman.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
