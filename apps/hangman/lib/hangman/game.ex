defmodule Hangman.Game do


  @type state :: map
  @type ch    :: binary
  @type optional_ch :: ch | nil

  @doc """
  Run a game of Hangman with our user. Use the dictionary to
  find a random word, and then let the user make guesses.

  If a paramter is supplied, it is used instead of a random word.
  This is used by the unit tests.
  """


  @spec new_game() :: state
  def new_game() do
    board = %{
      0 => %{0 => "x", 1 => "x", 2 => "x", 3 => "x", 4 => "x", 5 => "x", 6 => "x", 7 => "x", 8 => "x", 9 => "x", 10 => "x", 11 => "x", 12 => "x", 13 => "x", 14 => "x"},
      1 => %{0 => "x", 1 => "x", 2 => "x", 3 => "x", 4 => "x", 5 => "x", 6 => "x", 7 => "x", 8 => "x", 9 => "x", 10 => "x", 11 => "x", 12 => "x", 13 => "x", 14 => "x"},
      2 => %{0 => "x", 1 => "x", 2 => "x", 3 => "x", 4 => "x", 5 => "x", 6 => "x", 7 => "x", 8 => "x", 9 => "x", 10 => "x", 11 => "x", 12 => "x", 13 => "x", 14 => "x"},
      3 => %{0 => "x", 1 => "x", 2 => "x", 3 => "x", 4 => "x", 5 => "x", 6 => "x", 7 => "x", 8 => "x", 9 => "x", 10 => "x", 11 => "x", 12 => "x", 13 => "x", 14 => "x"},
      4 => %{0 => "x", 1 => "x", 2 => "x", 3 => "x", 4 => "x", 5 => "x", 6 => "x", 7 => "x", 8 => "x", 9 => "x", 10 => "x", 11 => "x", 12 => "x", 13 => "x", 14 => "x"},
      5 => %{0 => "x", 1 => "x", 2 => "x", 3 => "x", 4 => "x", 5 => "x", 6 => "x", 7 => "x", 8 => "x", 9 => "x", 10 => "x", 11 => "x", 12 => "x", 13 => "x", 14 => "x"},
      6 => %{0 => "x", 1 => "x", 2 => "x", 3 => "x", 4 => "x", 5 => "x", 6 => "x", 7 => "x", 8 => "x", 9 => "x", 10 => "x", 11 => "x", 12 => "x", 13 => "x", 14 => "x"},
      7 => %{0 => "x", 1 => "x", 2 => "x", 3 => "x", 4 => "x", 5 => "x", 6 => "x", 7 => "x", 8 => "x", 9 => "x", 10 => "x", 11 => "x", 12 => "x", 13 => "x", 14 => "x"},
      8 => %{0 => "x", 1 => "x", 2 => "x", 3 => "x", 4 => "x", 5 => "x", 6 => "x", 7 => "x", 8 => "x", 9 => "x", 10 => "x", 11 => "x", 12 => "x", 13 => "x", 14 => "x"},
      9 => %{0 => "x", 1 => "x", 2 => "x", 3 => "x", 4 => "x", 5 => "x", 6 => "x", 7 => "x", 8 => "x", 9 => "x", 10 => "x", 11 => "x", 12 => "x", 13 => "x", 14 => "x"},
      10 => %{0 => "x", 1 => "x", 2 => "x", 3 => "x", 4 => "x", 5 => "x", 6 => "x", 7 => "x", 8 => "x", 9 => "x", 10 => "x", 11 => "x", 12 => "x", 13 => "x", 14 => "x"},
      11 => %{0 => "x", 1 => "x", 2 => "x", 3 => "x", 4 => "x", 5 => "x", 6 => "x", 7 => "x", 8 => "x", 9 => "x", 10 => "x", 11 => "x", 12 => "x", 13 => "x", 14 => "x"},
      12 => %{0 => "x", 1 => "x", 2 => "x", 3 => "x", 4 => "x", 5 => "x", 6 => "x", 7 => "x", 8 => "x", 9 => "x", 10 => "x", 11 => "x", 12 => "x", 13 => "x", 14 => "x"},
      13 => %{0 => "x", 1 => "x", 2 => "x", 3 => "x", 4 => "x", 5 => "x", 6 => "x", 7 => "x", 8 => "x", 9 => "x", 10 => "x", 11 => "x", 12 => "x", 13 => "x", 14 => "x"},
      14 => %{0 => "x", 1 => "x", 2 => "x", 3 => "x", 4 => "x", 5 => "x", 6 => "x", 7 => "x", 8 => "x", 9 => "x", 10 => "x", 11 => "x", 12 => "x", 13 => "x", 14 => "x"}
    }
  end

  def set_black_pos(myCol, row, board) do
    board = put_in board[myCol][row], "b"
  end

  def set_white_pos(myCol, row, board) do
    board = put_in board[myCol][row], "w"
  end

  def black_win_the_game(myCol, row, count1, board) do
    if(board[myCol+1][row] == "b") do
      count1++
      if(count1 == 5)do
          :win
      end
      black_win_the_game(myCol+1, row, count1, board)
    end
   if(board[myCol-1][row] == "b")do
      count1++
      if(count1 == 5)do
         :win
      end
      black_win_the_game(myCol-1, row, count1, board)
    end
###########################################
    if(board[myCol][row+1] == "b")do
      count1++
      if(count1 == 5)do
        :win
      end
      black_win_the_game(myCol, row+1, count1, board)
    end
   if(board[myCol][row-1] == "b")do
      count1++
      if(count1 == 5)do
        :win
      end
      black_win_the_game(myCol, row-1, count1, board)
    end
#############################################
  if(board[myCol+1][row+1] == "b")do
    count1++
    if(count1 == 5)do
      :win
    end
    black_win_the_game(myCol+1, row+1, count1, board)
    end
   if(board[myCol-1][row-1] == "b")do
      count1++
      if(count1 == 5)do
        :win
      end
      black_win_the_game(myCol-1, row-1, count1, board)
    end
#############################################
  if(board[myCol+1][row-1] == "b")do
    count1++
    if(count1 == 5)do
      :win
    end
    black_win_the_game(myCol+1, row-1, count1, board)
  end
 if(board[myCol-1][row+1] == "b")do
      count1++
      if(count1 == 5)do
        :win
      end
      black_win_the_game(myCol-1, row+1, count1, board)
    end
end


def white_win_the_game(myCol, row, count2, board) do
  if(board[myCol+1][row] == "w")do
    count2++
    if(count2 == 5)do
      :win
    end
    black_win_the_game(myCol+1, row, count2, board)
  end
 if(board[myCol-1][row] == "w")do
    count2++
    if(count2 == 5)do
      :win
    end
    black_win_the_game(myCol-1, row, count2, board)
  end
###########################################
  if(board[myCol][row+1] == "w")do
    count2++
    if(count2 == 5)do
      :win
    end
    black_win_the_game(myCol, row+1, count2, board)
  end
 if(board[myCol][row-1] == "w")do
    count2++
    if(count2 == 5)do
      :win
    end
    black_win_the_game(myCol, row-1, count2, board)
  end
#############################################
if(board[myCol+1][row+1] == "w")do
  count2++
  if(count2 == 5)do
    :win
  end
  black_win_the_game(myCol+1, row+1, count2, board)
  end
 if(board[myCol-1][row-1] == "w")do
    count2++
    if(count2 == 5)do
      :win
    end
    black_win_the_game(myCol-1, row-1, count2, board)
  end
#############################################
if(board[myCol+1][row-1] == "w")do
  count2++
  if(count2 == 5)do
    :win
  end
  black_win_the_game(myCol+1, row-1, count2, board)
end
 if(board[myCol-1][row+1] == "w")do
    count2++
    if(count2 == 5)do
      :win
    end
    black_win_the_game(myCol-1, row+1, count2, board)
  end
end


end
