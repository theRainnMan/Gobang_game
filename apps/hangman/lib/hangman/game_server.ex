defmodule Hangman.GameServer do

  alias Hangman.Game, as: Impl

  use GenServer

  @name :hangman

  def start_link() do
    GenServer.start_link(__MODULE__, Hangman.Game.new_game, name: :game)
  end

  def set_black_pos(myCol, row) do
    GenServer.call(:game, { :set_black_pos, myCol, row })
  end

  def set_white_pos(myCol, row) do
    GenServer.call(:game, { :set_white_pos, myCol, row })
  end

  def black_win_the_game(myCol, row) do
    GenServer.call(:game, { :black_win_the_game, myCol, row})
  end

  def white_win_the_game(myCol, row) do
    GenServer.call(:game, { :white_win_the_game, myCol, row})
  end


#  def word_as_string(pid, reveal \\ false) do
#    GenServer.call(pid, { :word_as_string, reveal })
#  end

  # used for testing

#  def crash(pid, :normal) do
#    if Process.whereis(pid), do: GenServer.stop(pid)
#  end

#  def crash(pid, reason) do
#    GenServer.cast(pid, { :crash, reason })
#  end

  ###########################
  # end of public interface #
  ###########################


  def handle_call({ :set_black_pos, myCol, row }, _from, board) do

    { :reply, :ok, Impl.set_black_pos(myCol, row, board) }

#    { game, result, _guess } = Impl.set_black_pos(game, myCol, row, board)
#    { :reply, result, game }
  end

  def handle_call({ :set_white_pos, myCol, row }, _from, board) do
    { :reply, :ok, Impl.set_white_pos(myCol, row, board) }
  end

  def handle_call({ :black_win_the_game, myCol, row}, _from, board) do
    { :reply, Impl.black_win_the_game(myCol, row, 0, board), board }
  end

  def handle_call({ :white_win_the_game, myCol, row}, _from, board) do
    { :reply, Impl.white_win_the_game(myCol, row, 0, board), board }
  end

#  def handle_call({ :word_as_string, reveal }, _from, game) do
#    { :reply, Impl.word_as_string(game, reveal), game }
#  end

  # used for testing



end
